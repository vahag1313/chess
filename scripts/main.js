var cells = document.getElementsByClassName("cell");
var selectedCell;
var whiteTurn = true;

for(var i=0;i<cells.length;i++)
{
    cells[i].name = ((i)%8+1).toString()+parseInt(i/8+1).toString();
    cells[i].addEventListener("click",clickHandler);
}

function clickHandler() {
    var x = getX(this);
    var y = getY(this);
    console.log("Row: "+y+" Column: "+x);
    selectCell(this)
}

function getCell(x,y) {
    //return getCellStr(x.toString()+y.toString());
    return cells[(y-1)*8+x-1];
}

function getX(cell) {
    return parseInt(cell.name.slice(0,1));
}

function getY(cell) {
    return parseInt(cell.name.slice(1,2));
}

function selectCell(cell)
{
    if(selectedCell==undefined)
    {
        if(cell.firstChild!=null) {
            if (whiteTurn && cell.firstChild.alt == "white") {
                selectedCell = cell;
                cell.style.opacity = 0.5;
            } else if (!whiteTurn && cell.firstChild.alt == "black") {
                selectedCell = cell;
                cell.style.opacity = 0.5;
            }
        }
    }else
    {
        if(whiteTurn)
        {
            if(cell.firstChild == undefined || cell.firstChild.alt=="black")
            {
                if(stepCheck(selectedCell.firstChild.title,cell,selectedCell)) {
                    cell.innerHTML = selectedCell.innerHTML;
                    cell.style.opacity = 1;
                    selectedCell.innerHTML = "";
                    selectedCell = undefined;
                    whiteTurn = !whiteTurn;
                }
            }else
            if(cell.firstChild.alt == "white")
            {
                selectedCell.style.opacity=1;
                selectedCell = cell;
                selectedCell.style.opacity = 0.5;
            }
        }else
        {
            if(cell.firstChild == undefined || cell.firstChild.alt=="white")
            {
                if(stepCheck(selectedCell.firstChild.title,cell,selectedCell)) {
                    cell.innerHTML = selectedCell.innerHTML;
                    cell.style.opacity = 1;
                    selectedCell.innerHTML = "";
                    selectedCell = undefined;
                    whiteTurn = !whiteTurn;
                }
            }else
            if(cell.firstChild.alt == "black")
            {
                selectedCell.style.opacity=1;
                selectedCell = cell;
                selectedCell.style.opacity = 0.5;
            }
        }
    }
}

function stepCheck(figureType,targetCell,currentCell)
{
    switch (figureType)
    {
        case "1"://Rook
            if(getX(targetCell)==getX(currentCell)) {
                if (getY(targetCell) < getY(currentCell)) {
                    for (i = getY(currentCell)-1; i > getY(targetCell); i--) {
                        if (getCell(getX(currentCell), i).firstChild != undefined)
                            return false;
                    }
                } else {
                    for (i = getY(currentCell) + 1; i < getY(targetCell); i++) {
                        if (getCell(getX(currentCell), i).firstChild != undefined)
                            return false;
                    }
                }
                return true;
            }else if(getY(targetCell)==getY(currentCell)) {
                if(getX(targetCell)<getX(currentCell)) {
                    for(i=getX(currentCell)-1;i>getX(targetCell);i--) {
                        if(getCell(i,getY(currentCell)).firstChild != undefined) {
                            return false;
                        }
                    }
                }else {
                    for(i=getX(currentCell)+1;i<getX(targetCell);i++) {
                        if(getCell(i,getY(currentCell)).firstChild != undefined){
                            return false;
                        }
                    }
                }
                return true;
            }

            break;
        case "2"://Knight
            if(Math.abs(getX(targetCell)-getX(currentCell))==2 && Math.abs(getY(targetCell)-getY(currentCell))==1)
               return true;
            return (Math.abs(getX(targetCell) - getX(currentCell)) == 1 && Math.abs(getY(targetCell) - getY(currentCell)) == 2);
            break;
        case "3"://Bishop
            if(Math.abs(getX(targetCell)-getX(currentCell))==Math.abs(getY(targetCell)-getY(currentCell)))
            {
                if(getX(targetCell)-getX(currentCell)>0)
                {
                    if(getY(targetCell)-getY(currentCell)>0)
                    {
                        for(i=1;i<getX(targetCell)-getX(currentCell);i++)
                        {
                            if(getCell(getX(currentCell)+i,getY(currentCell)+i).firstChild != undefined){
                                return false;
                            }
                        }
                    }else
                    {
                        for(i=1;i<getX(targetCell)-getX(currentCell);i++)
                        {
                            if(getCell(getX(currentCell)+i,getY(currentCell)-i).firstChild != undefined){
                                return false;
                            }
                        }
                    }
                }else
                {
                    if(getY(targetCell)-getY(currentCell)>0)
                    {
                        for(i=1;i<getX(currentCell)-getX(targetCell);i++)
                        {
                            if(getCell(getX(currentCell)-i,getY(currentCell)+i).firstChild != undefined){
                                return false;
                            }
                        }
                    }else
                    {
                        for(i=1;i<getX(currentCell)-getX(targetCell);i++)
                        {
                            if(getCell(getX(currentCell)-i,getY(currentCell)-i).firstChild != undefined){
                                return false;
                            }
                        }
                    }
                }
            }else
                return false;
            return true;
            break;
        case "4"://Queen
            if(getX(targetCell)==getX(currentCell)) {
                if (getY(targetCell) < getY(currentCell)) {
                    for (i = getY(currentCell)-1; i > getY(targetCell); i--) {
                        if (getCell(getX(currentCell), i).firstChild != undefined)
                            return false;
                    }
                } else {
                    for (i = getY(currentCell) + 1; i < getY(targetCell); i++) {
                        if (getCell(getX(currentCell), i).firstChild != undefined)
                            return false;
                    }
                }
            }else if(getY(targetCell)==getY(currentCell)) {
                if(getX(targetCell)<getX(currentCell)) {
                    for(i=getX(currentCell)-1;i>getX(targetCell);i--) {
                        if(getCell(i,getY(currentCell)).firstChild != undefined) {
                            return false;
                        }
                    }
                }else {
                    for(i=getX(currentCell)+1;i<getX(targetCell);i++) {
                        if(getCell(i,getY(currentCell)).firstChild != undefined){
                            return false;
                        }
                    }
                }
            }else
            if(Math.abs(getX(targetCell)-getX(currentCell))==Math.abs(getY(targetCell)-getY(currentCell)))
            {
                if(getX(targetCell)-getX(currentCell)>0)
                {
                    if(getY(targetCell)-getY(currentCell)>0)
                    {
                        for(i=1;i<getX(targetCell)-getX(currentCell);i++)
                        {
                            if(getCell(getX(currentCell)+i,getY(currentCell)+i).firstChild != undefined){
                                return false;
                            }
                        }
                    }else
                    {
                        for(i=1;i<getX(targetCell)-getX(currentCell);i++)
                        {
                            if(getCell(getX(currentCell)+i,getY(currentCell)-i).firstChild != undefined){
                                return false;
                            }
                        }
                    }
                }else
                {
                    if(getY(targetCell)-getY(currentCell)>0)
                    {
                        for(i=1;i<getX(currentCell)-getX(targetCell);i++)
                        {
                            if(getCell(getX(currentCell)-i,getY(currentCell)+i).firstChild != undefined){
                                return false;
                            }
                        }
                    }else
                    {
                        for(i=1;i<getX(currentCell)-getX(targetCell);i++)
                        {
                            if(getCell(getX(currentCell)-i,getY(currentCell)-i).firstChild != undefined){
                                return false;
                            }
                        }
                    }
                }
            }else
                return false;
            return true;
            break;
        case "5"://King
            return !(Math.abs(getX(currentCell) - getX(targetCell)) > 1 || Math.abs(getY(currentCell) - getY(targetCell)) > 1);

            break;
        case "6"://Pawn
            if(whiteTurn)
            {
                if(getY(currentCell)==7 && getY(targetCell)==5 && getCell(getX(currentCell),6).firstChild == undefined)
                    return true;
                else if(getY(targetCell)-getY(currentCell)==-1 && getX(targetCell)==getX(currentCell) && targetCell.firstChild==undefined)
                    return true;
                else if(Math.abs(getX(targetCell)-getX(currentCell))==1 && Math.abs(getY(targetCell)-getY(currentCell))==1 && targetCell.firstChild!=undefined)
                    return true;
            }else
            {
                if(getY(currentCell)==2 && getY(targetCell)==4 && getCell(getX(currentCell),3).firstChild == undefined)
                    return true;
                else if(getY(targetCell)-getY(currentCell)==1 && getX(targetCell)==getX(currentCell) && targetCell.firstChild==undefined)
                    return true;
                else if(Math.abs(getX(targetCell)-getX(currentCell))==1 && Math.abs(getY(targetCell)-getY(currentCell))==1 && targetCell.firstChild != undefined)
                    return true;
            }

            return false;
            break;
    }
}